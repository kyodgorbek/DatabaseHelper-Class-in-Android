# DatabaseHelper-Class-in-Android

private static class DataBaseHelper extends SQLiteOpenHelper {
	
	DatabaseHelper(Context context) {
             super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
	       db.execSQL("Create Table " + NOTES_TABLE_NAME + " ("
		+Notes.TITLe + "TEXT,"
		+Notes.TITLE + " TEXT,"
		+Notes.NOTE + "TEXT,"
		+Notes.CREATED_DATE + "INTEGER,"
		+Notes:MODIFIED_DATE + " INTEGER"
		+ ");");
		
	}
	
	//...
	
}
